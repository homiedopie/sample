
<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('User', array( 'çlass' => 'form-horizontal' )); ?>
<form class="form-horizontal ">
  <div class="row">
    <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-offset-1 col-xs-10">
      <div class="form-group">
        <label for="txtUsername" class="control-label">Username</label>
        <?php echo $this->Form->input('username', array( 'class' => 'form-control', 'placeholder' => 'Username', 'id' => 'txtUsername', 'label' => false)); ?>
      </div>
      <div class="form-group">
        <label for="txtPassword" class="control-label">Password</label>
        <?php echo $this->Form->input('password', array( 'class' => 'form-control', 'placeholder' => 'Password', 'id' => 'txtPassword', 'label' => false)); ?>
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-default" value="Login">
      </div>
    </div>
  </div>
<?php echo $this->Form->end(); ?>