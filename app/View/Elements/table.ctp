<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading clearfix">
  	<span class="pull-left">
  		     <?php echo $this->Html->link(
        '<i class="fa fa-plus"></i> &nbsp; Add Post',
        array('controller' => 'posts', 'action' => 'add'),
        array('class' => 'btn btn-primary btn-sm', 'escape' => false)
    ); ?>
  	</span>
  </div>
  <table class="table table-striped">
	    <tr>
	        <th class="text-center">Id</th>
	        <th class="text-center">Title</th>
	        <th class="text-center">Actions</th>
	        <th class="hidden-xs">Created</th>
	    </tr>
	    <?php foreach ($posts as $post): ?>
	    <tr>
	        <td><?php echo $post['Post']['id']; ?></td>
	        <td>
	            <?php echo $this->Html->link($post['Post']['title'],array('controller' => 'posts', 'action' => 'view', $post['Post']['id'])); ?>
	        </td>
	        <td class="text-center">
	            <?php
	                echo $this->Form->postLink(
	                    '',
	                    array('action' => 'delete', $post['Post']['id']),
	                    array('confirm' => 'Are you sure?', 'class' => 'fa fa-times fa-1')
	                );
	            ?>
	            <?php
	                echo $this->Html->link(
	                    '',
	                    array('action' => 'edit', $post['Post']['id']), array('class' => 'fa fa-pencil fa-1')
	                );
	            ?>
	        </td>
	        <td class="hidden-xs"><?php echo $post['Post']['created']; ?></td>
	    </tr>
	    <?php endforeach; ?>
    <?php unset($post); ?>  
	</table>	
</div>