<?php

class PostsController extends AppController {

	/**
	 * Call Helpers
	 * @var array
	 */
	public $helpers = array('Html', 'Form');

	/**
	 * Default load page
	 * @return [type] [description]
	 */
	public function index(){
		// $this->set('posts', $this->Post->getAllById('all'));
		//var_dump();
		$posts = $this->Post->getAllByUserId($this->Auth->user('id'));
		// var_dump($posts);
		$this->set('posts', $posts);
		// die();
	}
	public function view($id = null){
		if(!$id){
			throw new NotFoundException("No Id Found");
		}
		$post = $this->Post->findById($id);
		if(!$post){
			throw new NotFoundException("No Post found for Id");
		}
		$this->set('post', $post);
	}
	public function add() {
        if ($this->request->is('post')) {
            $this->Post->create();
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');	
            if ($this->Post->save($this->request->data)) {

                $this->Session->setFlash(__('Your post has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Unable to add your post.'));
        }
    }
    public function edit($id = null) {
    	
	    if (!$id) {
	        throw new NotFoundException(__('Invalid post id'));
	    }

	    $post = $this->Post->findById($id);

	    if (!$post) {
	        throw new NotFoundException(__('Post does not exist'));
	    }

	    //done after submission
	    if ($this->request->is(array('post', 'put'))) {
	        $this->Post->id = $id;
	        if ($this->Post->save($this->request->data)) {
	            $this->Session->setFlash(__('Your post has been updated.'));
	            return $this->redirect(array('action' => 'index'));
	        }
	        $this->Session->setFlash(__('Unable to update your post.'));
	    }
	    //if data has no record
	    if (!$this->request->data) {
	        $this->request->data = $post;
	    }
	}
	public function delete($id) {
	    if ($this->request->is('get')) {
	        throw new MethodNotAllowedException();
	    }

	    if ($this->Post->delete($id)) {
	        $this->Session->setFlash(
	            __('The post with id: %s has been deleted.', h($id))
	        );
	        return $this->redirect(array('action' => 'index'));
	    }
	}
	public function isAuthorized( $user ){
		
		if( $this->action === 'add' ){
			return true;
		}
		if( in_array($this->action, array('edit', 'delete')) ){
			$postId = (int) $this->request->params['pass'][0];// to be changed
			if( $this->Post->isOwnedBy( $postId, $user['id'] ) ){
				return true;
			}
		}
		
		return parent::isAuthorized( $user );
	}
}