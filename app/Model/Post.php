<?php 
App::uses('AppModel', 'Model');
class Post extends AppModel{
    public $validate = array(
        'title' => array(
            'rule' => 'notEmpty'
        ),
        'body' => array(
            'rule' => 'notEmpty'
        )
    );

    public function isOwnedBy( $post, $user ){
    	return $this->field( 'id', array('id' => $post, 'user_id' => $user) ) !== false;
    }
    public function getAllByUserId($user_id = 0)
    {
        return $this->findAllByUserId($user_id);
    }
}